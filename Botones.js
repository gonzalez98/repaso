let values = [30, 44, 48, 56, 92];

function Promedio(values){

    //Usando ciclos 
    //parseInt() => comprueba el primer argumento, una cadena, e intenta devolver un entero de la base especificada

    let Pro = 0;
    for(let i = 0; i < 5 ; i++){
        Pro= parseInt(Pro+values[i]);
    }
    Pro= Pro/5;
    console.log("Promedio: "+Pro);

    //El método reduce() aplica una función a un acumulador y a cada valor de una array (de izquierda a derecha) para reducirlo a un único valor.
    let Suma = values.reduce((a, b) => a + b, 0);
    let Prom = Suma / values.length; 

    console.log("Promedio: " + Prom);
}

function Menor(values){

    let Me= 1000000000000000000
    for (let i=0; i<=values.length; i++){
        if (Me>values[i]){
            Me=values[i]
        }
    }
    console.log("Numero menor: "+ Me)

    //Ya que todos los elementos son números, podemos usar Math.min.apply o Math.max.apply pasando null como primer argumento y el array como segundo.
    
    let Min=Math.min.apply(null, values);
    console.log("Numero menor: "+Min)

}

function Mayor(values){

    let Ma= null
    for (let i=0; i<=values.length; i++){
        if (Ma<values[i]){
            Ma=values[i]
        }
    }
    console.log("Numero mayor: "+Ma)

    let Max=Math.max.apply(null, values);
    console.log("Numero mayor: "+Max)
}